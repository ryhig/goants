package main

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	screenWidth = 480
	screenHeight = 360
	antNumber = 10
	maxColor = 4
)

func getColors(count int) [4]byte {
	switch count {
		case 1:
			return [4]byte {0x40, 0xff, 0xDC, 0xFF}
		case 2:
			return [4]byte {0x00, 0xA9, 0xD4, 0xFF} 
		case 3:
			return [4]byte {0x1C, 0x31, 0x66, 0xFF}
		case 4:
			return [4]byte {0x24, 0x00, 0x47, 0xFF}
		default:
			return [4]byte {0x1C, 0x00, 0x21, 0xFF}
	}
}

type Game struct{
	pixels []byte
	w *World
}

func (g *Game) Update() error {
	g.w.Update()

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	if g.pixels == nil {
		g.pixels = make([]byte, screenWidth*screenHeight*4)
	}
	g.w.Draw(g.pixels)
	screen.WritePixels(g.pixels)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight 
}

func main() {
	ebiten.SetWindowSize(screenWidth*2, screenHeight*2)
	ebiten.SetWindowTitle("Hello, World!")
	g := &Game{
		w: newWorld(screenWidth, screenHeight, antNumber),
	}
	if err := ebiten.RunGame(g); err != nil {
		log.Fatal(err)
	}
}
