package main


type Ant struct {
	x int
	y int
	location int
	direction int
}
// Are we wasting 2 bits?. 0xff000000 RGBA.
func (a *Ant) Draw(pixels []byte) {
		pixels[a.location*4] = 0xff
		pixels[a.location*4 + 1] = 0x00
		pixels[a.location*4 + 2] = 0x00
		pixels[a.location*4 + 3] = 0x00
}

func (a *Ant) Update(area []int, width, height int) {
	// Change color of square we are on.
	if area[a.location] == maxColor { 
		area[a.location] = 0
	} else {
		area[a.location]++
	}
	//area[a.location] = !area[a.location]

	// I set 0 to North. 1 to W, 2 to S, 3 to E.
	// Below is a visual representation of this...
	//				N
	//				0
	//				|
	//		 W 1----|----3 E
	//				|
	//				2
	//				S
	// If area[a.location] the square is "true" meaning we're going clockwise.
	// if area[a.location] is not true we're going counter-clockwise.
	divisibleByTwo := area[a.location] % 2 == 0
	if divisibleByTwo && a.direction == 0 {
		a.direction = 3
	} else if divisibleByTwo {
		a.direction--
	} else if a.direction == 3 {
		a.direction = 0
	} else {
		a.direction++
	}
	switch a.direction {
		// Ant is going north.
		case 0:
			a.y--
		// Ant is going west.
		case 1:
			a.x--
		// Ant is going south.
		case 2:
			a.y++
		// Ant is going east.
		case 3:
			a.x++
	}

	// Handling when the x or y go out of bounds...
	// These if statements will make the ants wrap.
	if a.x < 0 {
		a.x = width - 1
	}
	if a.x > width - 1 {
		a.x = 0
	}
	if a.y < 0 {
		a.y = height - 1
	}
	if a.y > height - 1 {
		a.y = 0
	}

	a.location = a.y * width + a.x 
}
