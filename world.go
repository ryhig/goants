package main

import "math/rand"

type World struct {
	area []int
	width int
	height int
	ants []Ant
	maxAnts int
}

func newWorld(width, height, maxAnts int) *World {
	w := &World {
		area:	make([]int, width* height),
		width: width,
		height: height,
		ants: make([]Ant, maxAnts),
		maxAnts: maxAnts,
	}
	w.init()

	return w
}

func (w *World) init() {
	for i := range w.area {
		w.area[i] = 0
	}
	for i := 0; i < w.maxAnts; i++ {
		// initializing our ants randomly.
		w.ants[i].x = rand.Intn(w.width)
		w.ants[i].y = rand.Intn(w.height)
		// We're defaulting to a white square on initializing...
		w.area[w.ants[i].y*w.width + w.ants[i].x] = 1
		// our area is defined as one dimensional array.
		// the location variable takes the random y and x values 
		// to create our location in the one dimensional array, area.
		w.ants[i].location = w.ants[i].y*w.width + w.ants[i].x
		w.ants[i].direction = rand.Intn(4);
	}
}

func (w *World) Draw(pixels []byte) {


	// white if true or 0xffffffff, again RGBA.
	for i, v := range w.area {
		color := getColors(v)
		pixels[4*i] = color[0]
		pixels[4*i + 1] = color[1]
		pixels[4*i + 2] = color[2]
		pixels[4*i + 3] = color[3]
		//if v {
		//	pixels[4*i] = 0xff
		//	pixels[4*i+1] = 0xff
		//	pixels[4*i+2] = 0xff
		//	pixels[4*i+3] = 0xff
		//} else {
		//	// 0x0000000 if "false". RGBA.
		//	pixels[4*i] = 0
		//	pixels[4*i + 1] = 0
		//	pixels[4*i + 2] = 0
		//	pixels[4*i + 3] = 0
		//}
	}
	for _, a := range w.ants {
		a.Draw(pixels)
	}
}

func (w *World) Update() {
	for i := 0; i < w.maxAnts; i++ {
		w.ants[i].Update(w.area, w.width, w.height)
	}
}

